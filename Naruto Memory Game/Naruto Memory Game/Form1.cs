﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Naruto_Memory_Game
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        PictureBox[] cards;
        Image[] pics = new Image[18];
        PictureBox pressPic1;
        int ind1, ind2, clickNum = 0, count = 0, downTime = 0, score = 0, downTimeTick;
        Random rnd = new Random();
        private void cmbLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbLevel.SelectedIndex)
            {
                case 0:
                    cases(12, 3, 100, 100);
                    downTime = 40;
                    break;
                case 1:
                    cases(16, 4, 100, 100);
                    downTime = 100;
                    break;
                case 2:
                    cases(36, 6, 50, 35);
                    downTime = 600;
                    break;
            }
        }
        private void dis()
        {
            if(cards != null)
                for (int i = 0; i < cards.Length; i++)
                    cards[i].Visible = false;
        }
        private void shuffle(PictureBox[] arr)
        {
            int k = 1, tmp;
            for (int i = 0; i < cards.Length; i += 2)
            {
                cards[i].Tag = k;
                cards[i + 1].Tag = k;
                k++;
            }
            for(int i = 0; i < cards.Length - 1; i++)
            {
                k = rnd.Next(i, cards.Length);
                tmp = int.Parse(cards[k].Tag.ToString());
                cards[k].Tag = int.Parse(cards[i].Tag.ToString());
                cards[i].Tag = tmp;
            }
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            downTimeTick = downTime;
            countDown.Enabled = true;
        }
        private void countDown_Tick(object sender, EventArgs e)
        {
            lblCountDown.Text = Convert.ToString(downTimeTick);
            if (downTimeTick > 0)
                downTimeTick--;
        }
        private void cases(int num, int div, int x, int y)
        {
            count = 0;
            dis();
            cards = new PictureBox[num];
            for (int i = 0; i < cards.Length; i++)
            {
                cards[i] = new PictureBox();
                cards[i].SizeMode = PictureBoxSizeMode.StretchImage;
                cards[i].Size = new Size(160, 160);
                cards[i].Location = new Point(x, y);
                cards[i].Visible = true;
                cards[i].BorderStyle = BorderStyle.Fixed3D;
                cards[i].Click += cards_Click;
                if(num == 36)
                {
                    cards[i].Size = new Size(140, 140);
                    if ((i + 1) % div == 0)
                    {
                        x = 50;
                        y += 170;
                    }
                    else
                        x += 200;
                }
                else
                {
                    if ((i + 1) % div == 0)
                    {      
                        x = 100;
                        y += 220;
                    }
                    else
                        x += 200;  
                }
                Controls.Add(cards[i]);
                cards[i].Image = Properties.Resources.cardb;
            }
            shuffle(cards);
            score = 0;
            lblScore.Text = "0";
        }
        private void cards_Click(object sender, EventArgs e)
        {
            if (countDown.Enabled == false)
            {
                MessageBox.Show("Please press the Start button first");
            }
            else
            {
                PictureBox pressPic = (PictureBox)sender;
                pressPic.Image = (Image)(Properties.Resources.ResourceManager.GetObject("p" + pressPic.Tag.ToString()));
                clickNum++;
                if (clickNum == 1)
                {
                    pressPic1 = (PictureBox)sender;
                    ind1 = int.Parse(pressPic.Tag.ToString());
                }
                if (clickNum == 2)
                {
                    ind2 = int.Parse(pressPic.Tag.ToString());
                    if (ind1 == ind2)
                    {
                        count++;
                        score++;
                        lblScore.Text = score + "";
                        pressPic.Update();
                        System.Threading.Thread.Sleep(1000);
                        pressPic1.Visible = false;
                        pressPic.Visible = false;
                    }
                    else
                    {
                        pressPic.Update();
                        System.Threading.Thread.Sleep(700);
                        pressPic1.Image = Properties.Resources.cardb;
                        pressPic.Image = Properties.Resources.cardb;
                    }
                    clickNum = 0;
                }
                if (cards.Length / 2 == count)
                {
                    countDown.Enabled = false;
                    MessageBox.Show("Congrats! you won the game in " + (downTime - int.Parse(lblCountDown.Text)) + " seconds!");
                }
            }
        }
    }
}